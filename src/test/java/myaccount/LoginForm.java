package myaccount;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginForm {
    public static Target USERNAME_FIELD = Target.the("username textbox")
            .located(By.id("username"));
    public static Target PASSWORD_FIELD = Target.the("password textbox")
            .located(By.id("password"));
    public static Target LOGIN_BUTTON = Target.the("login button")
            .locatedBy("input.woocommerce-Button.button");
    public static Target REMEMBER_ME_CHECKBOX = Target.the("remember me checkbox")
            .located(By.id("rememberme"));
    public static Target LOST_PASSWORD_LINK = Target.the("lost password link")
            .locatedBy("p.woocommerce-LostPassword.lost_password > a");
}
