package myaccount;


import net.serenitybdd.screenplay.targets.Target;

public class Woocommerce {
    public static Target ERROR_MESSAGE = Target.the("error label")
            .locatedBy("#page-36 > div > div.woocommerce > ul");
}
