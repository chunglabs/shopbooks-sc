package myaccount;

import cucumber.api.java.en.Given;
import section.Navigate;
import section.Section;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class StepDefs {
    @Given("^that (.*) navigated to My Account Page$")
    public void open_my_account_page(String actorName){
        theActorCalled(actorName).attemptsTo(Navigate.to(Section.MYACCOUNT_PAGE));
    }
}
