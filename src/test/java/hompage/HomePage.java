package hompage;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;

import java.util.List;

public class HomePage extends PageObject {
    public static Target SLIDERS = Target.the("sliders")
            .locatedBy("#n2-ss-6 > div.n2-ss-slider-1.n2-grab > div > div img");
    public static Target ARRIVALS = Target.the("arrivals")
            .locatedBy(".woocommerce .products");
    public static Target SHOP_MENU = Target.the("shop menu link")
            .locatedBy("#menu-item-40 > a");
    public static Target HOME_MENU = Target.the("home menu button")
            .locatedBy("");
    public static Target HOME_LINK = Target.the("home page link")
            .locatedBy("#content > nav > a");
}
