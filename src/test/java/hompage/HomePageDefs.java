package hompage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.screenplay.actions.Click;
import section.Navigate;
import section.Section;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

public class HomePageDefs {
    @Given("^that (.*) opened the Home page$")
    public void open_the_home_page(String actorName) throws Throwable{
        theActorCalled(actorName).attemptsTo(
                Navigate.to(Section.HOME_PAGE)
        );
    }
    @Then("^the Home page must contains only (\\d+) sliders$")
    public void the_home_page_must_contains_sliders(int number){
        theActorInTheSpotlight().should(seeThat(
                TheSliders.displayed(),hasSize(number)
        ));
    }
    @Then("^the Home page must contains only (\\d+) arrivals$")
    public void the_home_page_must_contains_arrivals(int number){
        theActorInTheSpotlight().should(seeThat(
                TheArrivals.displayed(),hasSize(number)
        ));
    }
}
