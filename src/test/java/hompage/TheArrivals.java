package hompage;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

public class TheArrivals implements Question<List<WebElementFacade>> {
    @Override
    public List<WebElementFacade> answeredBy(Actor actor) {
        return HomePage.ARRIVALS.resolveAllFor(actor);
    }
    public static Question<List<WebElementFacade>> displayed(){
        return new TheArrivals();
    }
}
