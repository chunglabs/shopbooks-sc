package section;

public enum Section {
    HOME_PAGE("http://practice.automationtesting.in/"),
    MYACCOUNT_PAGE("http://practice.automationtesting.in/my-account/")
    ;
    private String url;

    Section(String url) {
        this.url = url;
    }

    public String url() {
        return url;
    }

}
